import fs from 'fs';
import bluebird from 'bluebird';
import Prezi from '../server/models/presentation.js';
import config from '../server/configs/config.js';
import configMongo from '../server/configs/mongo.js'

bluebird.promisifyAll(fs);

configMongo();

fs.readFileAsync(`${config.root}/data/prezis.json`)
    .then(data => Prezi.insertMany(JSON.parse(data)))
    .catch(console.log)
    .finally(process.exit);
