import express from 'express';
import Prezi from '../models/presentation.js';

const router = express.Router();

router.get('/', (req, res) => {
    Prezi.find().then(data => res.json(data)).catch(err => res.json({error: err}));
});

export default router;
