import mongoose from 'mongoose';

const presentationSchema = mongoose.Schema({
    title: String,
    thumbnail: String,
    creator: {
        name: String,
        profileUrl: String
    },
    createdAt: { type: Date, default: Date.now }
});

export default mongoose.model('presentation', presentationSchema);
