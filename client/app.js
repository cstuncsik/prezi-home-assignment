import home from './components/home/home';
import not_found from './components/not_found/not_found';

page('/', home);
page('/home', home);
page('*', not_found);

$(() => {
    page();
});
