const showPage = page => $('#content').find('.page').addClass('hidden').removeClass('visible').filter(`.page-${page}`).removeClass('hidden').addClass('visible');

export default {showPage};
