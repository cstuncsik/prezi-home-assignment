import Rx from 'rxjs-es6/Rx';

const viewModel = {
    prezis: ko.observableArray(),
    desc: ko.observable(true),
    toggleOrderDir: function () {
        this.desc(!this.desc());
    }
};

viewModel.orderDir = ko.pureComputed(function () {
    return viewModel.desc() ? 'desc' : 'asc'
});

const list = () => $.ajax({
    url: '/api/prezis'
}).then(data => {
    data.forEach(obj => {
        obj.hidden = ko.observable(false);
        obj.displayDate = moment(obj.createdAt).format('ll');
    });
    viewModel.prezis.removeAll();
    viewModel.prezis.push.apply(viewModel.prezis, data);
});

const convertDateToTs = date => moment(date).valueOf();
const dateSort = dir => (a, b) => (convertDateToTs(a.createdAt) - convertDateToTs(b.createdAt)) * (dir === 'asc' ? 1 : -1);

const init = () => {
    ko.applyBindings(viewModel, document.querySelector('.module-prezis'));

    Rx.Observable.fromEvent(document.querySelector('#search'), 'keyup')
        .map(e => e.target.value)
        .filter(s => s.length > 2 || !s)
        .distinctUntilChanged()
        .subscribe(
            str => ko.utils.arrayForEach(viewModel.prezis(), prezi => prezi.hidden(prezi.title.indexOf(str) < 0)),
            err => console.log(err)
        );

    Rx.Observable.fromEvent(document.querySelector('#order'), 'click')
        .map(e => e.target.className)
        .subscribe(
            orderDir => viewModel.prezis.sort(dateSort(orderDir)),
            err => console.log(err)
        );
};

export default {init, list};
