import presentations from '../../modules/presentations/presentations';
import navigation from '../../modules/navigation/navigation';

presentations.init();

export default () => {
    presentations.list().then(() => navigation.showPage('home'));
};
